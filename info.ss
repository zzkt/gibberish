#lang setup/infotab

(define name
  "xmpp")
  
(define blurb 
  '("A library for the XMPP or Jabber protocol."))

(define categories 
  '(xml net))

(define homepage 
  "http://github.com/zzkt/gibberish/")

(define primary-file 
  "xmpp.ss")

(define repositories 
  '("4.x"))
  
(define required-core-version 
  "4.1.5")

(define release-notes 
  '(""))

(define scribblings 
  '(("xmpp.scrbl" ())))
